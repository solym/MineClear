#ifndef __SELECT_GAME_H__
#define __SELECT_GAME_H__
#include "customer_define.h"


class SelectGame :public LayerColor
{
public:
	CREATE_FUNC(SelectGame);
	SCENE_FUNC(SelectGame);
	bool init();
	//�ص�����
protected:
	void MenuEasyCallBack(Ref*);
	void MenuMidCallBack(Ref*);
	void MenuHardCallBack(Ref*);
	void MenuExitCallBack(Ref*);
};

#endif // !__SELECT_GAME_H__