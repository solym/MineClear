#ifndef __GAME_SCENE_H__
#define __GAME_SCENE_H__
#include "cocos2d.h"
USING_NS_CC;

class MineBlock :public Sprite
{
public:
	MineBlock() { _dig = false; }
	bool indirectInit() { return init(); }
	bool indirectInitWithTexture(cocos2d::Texture2D* t) { return initWithTexture(t); }
	void setValue(short v) { _value = (short)v; }
	int getValue() { return (int)_value; }
	bool getFlag() { return _flag; }
	void setFlag(bool flag) { _flag = flag; }
	bool getDig() { return _dig; }
	void setDig(bool dig) { _dig = dig; }
	bool callinitWithTexture(Texture2D* t) { return Sprite::initWithTexture(t); }
private:
	short _value;	//周围的雷数	,之前有char表示，在移植到安卓的时候出错
	bool _flag;		//是否被标记
	bool _dig;		//是否被挖开
};

class GameScene : public cocos2d::Layer
{
public:

	virtual bool init();

	//创建scene的时候选关，1==》9*9，2==》16*16,3==》24*24
	static cocos2d::Scene* scene(int customs = 1);
	//创建的时候就要确定
	static GameScene* create(int customs = 1);

	//Menu Call Back
	void MenuCallDig(Ref*);
	void MenuCallFlag(Ref*);
	void MenuCallRestart(Ref*);
	void MenuCallExit(Ref*);
	//override	ccTouch...
	bool callTouchBegan(Touch * pTouch , Event * pEvent);
	void callTouchEnded(Touch * pTouch , Event * pEvent);

protected:
	bool LoadResource();	//加载资源
	bool addBackGround();	//设置背景
	void addMenu();	//添加菜单

	bool BuryMine();	//初始化土地，埋雷
	bool InitMine();	//初始化雷区精灵
	bool ContinueGame();	//判断输赢,是否继续游戏

	void NoAroundMine(int row , int col , __Array * arr);	//周边无雷块的连带显示
	
	MineBlock * getRowColAddr(int r , int c);	//获取r行c列的地址
	const int getRowColValue(int r , int c);	//获取r行c列的值
private:
	int		_mine;			//地雷数
	int		_customs;		//关数
	int		_MaxRanks;		//最大可用土地行列
	int		_count;			//记录当前扫了多少块区域
	bool	_toFlag;		//是不是去标记雷块
	bool	_isContinue;	//判断是否继续游戏
	MineBlock*	_land[26 * 26];	//土地，放置地雷，,多出两行两列，为了统一操作
	Layer* _layerColor;	//背景遮罩层,安置雷区格子块
	__Array * _ArraySprite;	//存放精灵数组
	MenuItemImage * _dig;	//挖开
	MenuItemImage * _flag;	//标记，插旗
	MenuItemImage * _exitGame;	//结束游戏
	MenuItemImage * _restart;	//重来菜单按钮
	Sprite * _bgland;		//雷区背景精灵
	Sprite * _bgmenu;		//菜单项背景精灵
};



#endif // __GAME_SCENE_H__
