#ifndef __CUSTOMER_DEFINE_H__
#define __CUSTOMER_DEFINE_H__

#include "cocos2d.h"
USING_NS_CC;
/** scene函数的宏定义，创建一个scene，并addChild这个类型的对象地址（cocos2.2.3遗留的习惯)
* define a scene function for a specific type, such as CCLayer
* @__TYPE__ class type to add scene(), such as CCLayer
*/
#define SCENE_FUNC(__TYPE__) \
	static cocos2d::CCScene* scene() \
{ \
	cocos2d::CCScene* scene = cocos2d::CCScene::create(); \
	__TYPE__ * sprite = __TYPE__::create(); \
	scene->addChild(sprite); \
	return scene; \
} \


//cocos3.0之后使用cocos2d::log替代了CCLog,但是和数学函数log重名，难得去写cocs2d::
#define CCLog cocos2d::log
//获取窗口（屏幕）的大小，修改为3.0的用法了，原本是
//#define WinSize (cocos2d::CCDirector::sharedDirector()->getWinSize())
#define WinSize (cocos2d::Director::getInstance()->getWinSize())
//获取窗口的宽度
#define WinWidth  (cocos2d::Director::getInstance()->getWinSize().width)
//获取窗口的高度
#define WinHeigth  (cocos2d::Director::getInstance()->getWinSize().height)

#endif // !__CUSTOMER_DEFINE_H__
