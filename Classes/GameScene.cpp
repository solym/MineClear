#include "GameScene.h"
#include "customer_define.h"
#include "SelectGame.h"
GameScene* GameScene::create(int customs)
{
	GameScene *s = new GameScene();
	if (s != NULL) {
		if (customs<0 || customs >3) {
			CCLog("customs value error");
			return NULL;
		}
		s->_customs = customs;
		s->_count = 0;		//记录当前扫了多少块区域
		s->_toFlag = false;		//是不是去标记雷块
		s->init();
		s->autorelease();	//此处需要，在addChile的时候自然会被retain
	}
	else {
		CC_SAFE_DELETE(s);	//安全删除
		//#define CC_SAFE_DELETE(p)            do { if(p) { delete (p); (p) = 0; } } while(0)
		//等价于===>   do{  //使用do while是为了适应我们习惯性了加上分号，没有分号会提示错误
		//					if(s!=NULL) {
		//					delete s;
		//					s=NULL;
		//					}
		//				}while(0)
	}
	return s;
}


Scene* GameScene::scene(int customs)
{
	Scene *scene = Scene::create();

	GameScene *layer = GameScene::create(customs);

	scene->addChild(layer);

	return scene;
}

bool GameScene::init()
{
	//////////////////////////////
	// 1. super init first
	if (!CCLayer::init()) {
		return false;
	}
	//加载资源
	LoadResource();
	//设置背景
	addBackGround();
	//设置菜单项
	addMenu();
	//初始化土地，埋雷
	BuryMine();

	//初始化雷区精灵
	InitMine();

	//设置触摸事件
	//setTouchEnabled(true);
	//setTouchMode(kCCTouchesOneByOne);
	//setTouchPriority(2);

	//创建一个单点触摸
	auto evt = EventListenerTouchOneByOne::create();
	evt->setEnabled(true);
	evt->onTouchBegan = [&](Touch * pTouch , Event * pEvent){
		return this->callTouchBegan(pTouch , pEvent);
	};
	evt->onTouchEnded = [&](Touch * pTouch , Event * pEvent){
		return this->callTouchEnded(pTouch , pEvent);
	};
	_eventDispatcher->addEventListenerWithSceneGraphPriority(evt,this);
	return true;
}




/////////////////////////////////////////////////////////////////////////
//	初始化加载资源
bool GameScene::LoadResource()
{
	const char* ImgFile[] = {
		"0.png" ,
		"1.png" ,
		"2.png" ,
		"3.png" ,
		"4.png" ,
		"5.png" ,
		"6.png" ,
		"7.png" ,
		"8.png" ,
		"no.png" ,
		"-1.png" ,
		"bg_land.png" ,
		"bg_menu.png" ,
		"boom.png" ,
		"flag.png" ,
		"blink_1.png" ,
		"blink_2.png" ,
		"exit.png" ,
		"exits.png" ,
		"restart.png" ,
		"restarts.png"
	};
	for (int i = 0; i < sizeof(ImgFile) / sizeof(char*); ++i) {
		if (NULL == TextureCache::getInstance()->addImage(ImgFile[i])) {
			return false;
		}
	}
	return true;
}

//////////////////////////////////////////////////////////////////////
//	设置背景
bool GameScene::addBackGround()
{
	//创建一个土地放置层,其是一个CCSprite，因为要显示背景
	Texture2D * texture = TextureCache::getInstance()->getTextureForKey("bg_land.png");
	if (!texture) {
		return false;
	}
	//设置雷区背景
	Sprite * bgland = Sprite::createWithTexture(texture);
	//设置背景为宽度适应
	bgland->setScale(WinWidth / bgland->getContentSize().width);
	//零点为锚点
	bgland->setAnchorPoint(Vec2::ZERO);
	addChild(bgland);
	bgland->setPosition(Vec2(0 , WinHeigth - bgland->getBoundingBox().size.height));	//放置在上半屏幕
	_bgland = bgland;
	bgland = nullptr;
	//设置菜单背景
	texture = TextureCache::getInstance()->getTextureForKey("bg_menu.png");
	Sprite * bgmenu = Sprite::createWithTexture(texture);
	//设置缩放宽度为适应屏幕宽度
	bgmenu->setScaleX(WinWidth / bgmenu->getContentSize().width);
	//设置高度为适应除去_bgland后剩下的部分
	bgmenu->setScaleY(_bgland->getBoundingBox().getMinY() / bgmenu->getContentSize().height);

	bgmenu->setAnchorPoint(Vec2::ZERO);
	bgmenu->setPosition(Vec2::ZERO);
	addChild(bgmenu);
	_bgmenu = bgmenu;
	bgmenu = nullptr;
	//添加了一个半透明的Layer，到时候用来防止雷区的格子块
	//本来是想放置在Sprite上的，算了，还是这个比较实在
	//加载一个半透明的CCLayerColor,来遮住背景
	_layerColor = LayerColor::create(Color4B(128 , 128 , 128 , 64) ,
									   WinWidth ,	//设置其大小和雷区背景一致
									   _bgland->getBoundingBox().size.height);
	_layerColor->setPosition(_bgland->getPosition());
	addChild(_layerColor);
	//设置触摸优先级	这个没有用，就没有在这里设置触摸事件
	//_layerColor->setTouchPriority(10);
	return true;
}

////////////////////////////////////////////////////////////////////////
//	添加菜单项
void GameScene::addMenu()
{
	//创建四个菜单项
	_dig = CCMenuItemImage::create("dig_button.png" ,
								   "digs_button.png" ,
								   "dign_button.png" ,
								   this ,
								   menu_selector(GameScene::MenuCallDig));
	_flag = CCMenuItemImage::create("flag_button.png" ,
									"flags_button.png" ,
									"flagn_button.png" ,
									this ,
									menu_selector(GameScene::MenuCallFlag));
	_exitGame = CCMenuItemImage::create("mainmenu2.png" ,
										"mainmenu2_s.png" ,
										this ,
										menu_selector(GameScene::MenuCallExit));
	_restart = CCMenuItemImage::create("restart.png" ,
									   "restarts.png" ,
									   this ,
									   menu_selector(GameScene::MenuCallRestart));
	//用四个菜单项初始化四个菜单
	Menu *menu = Menu::create(_dig , _flag , _exitGame , _restart , NULL);
	//菜单项添加到_bgmenu上去会比较好还是直接添加到当前对象上，这个问题以后再说
	_bgmenu->addChild(menu);
	menu->setPosition(Vec2::ZERO);

	float lowWidth = _bgmenu->getContentSize().width;
	float lowHeight = _bgmenu->getContentSize().height;
	//设置菜单项的大小，宽度适应为窗口宽度的1/3
	_dig->setScale(lowWidth / 3.0f / _dig->getContentSize().width);
	_flag->setScale(lowWidth / 3.0f / _dig->getContentSize().width);
	_exitGame->setScale(lowWidth / 3.0f / _dig->getContentSize().width);
	_restart->setScale(lowWidth / 3.0f / _dig->getContentSize().width);
	//设置锚点,中心点
	_dig->setAnchorPoint(Vec2(.5f , .5f));
	_flag->setAnchorPoint(Vec2(.5f , .5f));
	_exitGame->setAnchorPoint(Vec2(.5f , .5f));
	_restart->setAnchorPoint(Vec2(.5f , .5f));
	//设置放置的位置
	
	_dig->setPosition(Vec2(lowWidth * 0.25f , (lowHeight)* 0.75f));
	_flag->setPosition(Vec2(lowWidth * 0.75f , (lowHeight)* 0.75f));
	_exitGame->setPosition(Vec2(lowWidth * 0.75f , (lowHeight)* 0.25f));
	_restart->setPosition(Vec2(lowWidth * 0.25f , (lowHeight)* 0.25f));
	_dig->setEnabled(false);

}

//////////////////////////////////////////////////////////////////////////
//	Menu Call Back
void GameScene::MenuCallDig(Ref*)
{
	_dig->setEnabled(false);
	_flag->setEnabled(true);
	_toFlag = false;
}

void GameScene::MenuCallFlag(Ref*)
{
	_dig->setEnabled(true);
	_flag->setEnabled(false);
	_toFlag = true;
}

void GameScene::MenuCallRestart(Ref*)
{
	Director::getInstance()->replaceScene(GameScene::scene(_customs));
}

void GameScene::MenuCallExit(Ref*)
{	//此处并不是退出，而是切换到选关界面
	Director::getInstance()->replaceScene(SelectGame::scene());
	//CCDirector::sharedDirector()->end();
}

////////////////////////////////////////////////////////////////////////
//	获取行列坐标上的雷区块的精灵的地址
MineBlock * GameScene::getRowColAddr(int r , int c)
{
	assert(r >= 0 && r < 26 && c >= 0 && c < 26);
	return _land[r * 26 + c];	//第r行c列的地址
}
//	获取行列坐标上的雷区块的精灵的属性值
const int GameScene::getRowColValue(int r , int c)
{	//r和c的值应该在[1,24]是土地的
	assert(r >= 0 && r < 26 && c >= 0 && c < 26);
	return _land[r * 26 + c]->getValue();	//第r行c列的值
}

//////////////////////////////////////////////////////////
//	埋雷
bool GameScene::BuryMine()
{
	switch (_customs) {
	case 1:
		_MaxRanks = 9;
		_mine = 10;
		break;
	case 2:
		_MaxRanks = 16;
		_mine = 48;
		break;
	case 3:
		_MaxRanks = 24;
		_mine = 96;
		break;
	}

	//创建雷块,并初始化其值为0
	for (int r = 0; r <= _MaxRanks + 1; ++r) {
		for (int c = 0; c <= _MaxRanks + 1; ++c) {
			_land[r * 26 + c] = new MineBlock();
			_land[r * 26 + c]->setValue(0);
		}
	}

	//创建雷区的地雷
	srand((unsigned int)time(NULL));

	int i = 0;
	int row = 1 , col = 1;
	while (i < _mine) {		//随机设置雷的位置
		row = rand() % _MaxRanks + 1;
		col = rand() % _MaxRanks + 1;
		if (getRowColValue(row , col) != 0) {
			continue;
		}
		getRowColAddr(row , col)->setValue(-1);	//雷的代号是-1
		//		CCLog("row = %d col = %d [%d]" , row , col , getRowColValue(row , col));
		++i;
	}
	//计算非雷块的周围的雷的数量，得到其值
	int sum = 0;
	for (int r = 1; r <= _MaxRanks; ++r) {
		for (int c = 1; c <= _MaxRanks; ++c) {
			if (getRowColValue(r , c) == -1) {
				continue;
			}
			sum = 0;
			sum += getRowColValue(r + 1 , c - 1) == -1 ? 1 : 0;	//	r-1,c-1		r-1,c	r-1,c+1
			sum += getRowColValue(r + 1 , c) == -1 ? 1 : 0;		//	r,c-1		r,c		r,c+1
			sum += getRowColValue(r + 1 , c + 1) == -1 ? 1 : 0;	//	r+1,c-1		r+1,c	r+1,c+1
			sum += getRowColValue(r , c - 1) == -1 ? 1 : 0;
			//sum += getRowColValue(r , c) == -1 ? 1 : 0;
			sum += getRowColValue(r , c + 1) == -1 ? 1 : 0;
			sum += getRowColValue(r - 1 , c - 1) == -1 ? 1 : 0;
			sum += getRowColValue(r - 1 , c) == -1 ? 1 : 0;
			sum += getRowColValue(r - 1 , c + 1) == -1 ? 1 : 0;
			//设置land格子周边的雷数
			getRowColAddr(r , c)->setValue(sum);
		}
	}
	return true;
}

//////////////////////////////////////////////////////////////////
//	初始化雷区精灵
bool GameScene::InitMine()
{
	//初始化精灵保存数组
	_ArraySprite = __Array::create();
	if (!_ArraySprite) {
		return false;
	}
	_ArraySprite->retain();
	//获取精灵尺寸
	float w = WinWidth / _MaxRanks;

	//初始化雷区精灵
	for (int r = 0; r <= _MaxRanks + 1; ++r) {
		for (int c = 0; c <= _MaxRanks + 1; ++c) {

			//全部都初始化为未踩的状态
			MineBlock * mb = getRowColAddr(r , c);
			if (!mb) {
				return false;
			}

			mb->indirectInitWithTexture(CCTextureCache::getInstance()->getTextureForKey("no.png"));
			mb->setValue(getRowColValue(r , c));
			mb->setFlag(false);	//设置为未标记状态
			if (r == 0 || c == 0 || r == _MaxRanks + 1 || c == _MaxRanks + 1) {
				mb->retain();	//保留下来，但是不加入到游戏场景中
				continue;
			}

			addChild(mb);	//加载精灵到游戏场景层
			mb->setScale(w / mb->getContentSize().width);
			mb->setAnchorPoint(Vec2(0.0f , .0f));
			//设置精灵挂载的位置
			mb->setPosition(Vec2((r - 1)*w , (c - 1)*w) + _layerColor->getPosition());
			//添加到数组
			_ArraySprite->addObject(mb);
		}
	}
	return true;
}

///////////////////////////////////////////////////////////////
//	游戏输赢判断，是否继续游戏
bool GameScene::ContinueGame()
{
	if (_count == -1) {
		LabelTTF* ttf = LabelTTF::create("Your fail!" , "Felt" , WinWidth / 10);
		_bgland->setLocalZOrder(100);
		_bgland->addChild(ttf);
		ttf->setPosition(Vec2(WinWidth / 3 , 50));
		ttf->setColor(Color3B(255 , 0 , 0));

		return false;
	}
	if (_count == _MaxRanks*_MaxRanks - _mine) {
		Texture2D * texture1 = TextureCache::getInstance()->addImage("blink_1.png");
		Texture2D * texture2 = TextureCache::getInstance()->addImage("blink_2.png");
		Animation * animation = Animation::create();

		animation->addSpriteFrameWithTexture(texture1 ,		//设置动画的矩形的定位点
											 Rect(.0f , .0f
											 , texture1->getContentSize().width , texture1->getContentSize().height));
		animation->addSpriteFrameWithTexture(texture2 ,
											 Rect(.0f , .0f ,
											 texture2->getContentSize().width ,texture1->getContentSize().height));
		animation->setDelayPerUnit(0.1f);

		Animate *animate = Animate::create(animation);

		_bgland->setLocalZOrder(100);
		_bgland->runAction(CCRepeatForever::create(animate));
		//适应_bglang的显示高度来缩放
		_bgland->setScale(_bgland->getBoundingBox().size.height / texture1->getContentSize().height);
		return false;	//胜利了
	}
	return true;
}

/////////////////////////////////////////////////////////////////////////
//	ccTouch
bool GameScene::callTouchBegan(Touch * pTouch , Event * pEvent)
{
	return _isContinue;	//如果不再继续游戏，就不再往下传递了
}

//	Touch	判断点击的位置以及更新图片
void GameScene::callTouchEnded(Touch * pTouch , Event * pEvent)
{
	Point pt = pTouch->getLocation();
	//	CCLog("pt.x = %f\t pt.y = %f" , pt.x , pt.y);
	Ref * obj;

	CCARRAYDATA_FOREACH(_ArraySprite->data , obj)
	{
		MineBlock * mb = (MineBlock*)obj;
		//如果点击的这个像素不在这个精灵所在的矩形范围内
		if (!mb->getBoundingBox().intersectsRect(CCRectMake(pt.x , pt.y , 1 , 1))) {
			continue;	//	下一个
		}
		//	点击的位置在这个精灵的范围之内
		if (mb->getDig()) {
			return;		//已经被挖开
		}
		char filename[16] = { 0 };
		int t = mb->getValue();
		//	如果是去标记
		if (_toFlag) {
			const char *ptx = mb->getFlag() ? "no.png" : "flag.png";
			mb->setTexture(CCTextureCache::getInstance()->getTextureForKey(ptx));
			mb->setFlag(!mb->getFlag());
			return;
		}
		//踩到雷------------------------------------------------------------------------
		if (t == -1) {
			_count = -1;	//设置计数为-1，代表输了
			//显示所有雷
			CCARRAYDATA_FOREACH(_ArraySprite->data , obj)
			{
				MineBlock * mine = (MineBlock*)obj;
				if (mine->getValue() == -1) {
					mine->setTexture(TextureCache::getInstance()->getTextureForKey("-1.png"));
				}
			}
			//显示爆炸
			mb->setTexture(TextureCache::getInstance()->getTextureForKey("boom.png"));
			goto CONTINUEGAME;	//到此结束
		}	// end if t == -1

		//没有踩到雷，但是周围有雷
		if (t>0) {
			//在移植到安卓的时候，全部精灵的_value都为0，出现错误，sprintf函数没有问题
			sprintf(filename , "%d.png" , t);
			mb->setTexture(TextureCache::getInstance()->getTextureForKey(filename));
			mb->setDig(true);
			++_count;	//扫区计数加一
			goto CONTINUEGAME;	//到此结束
		}

		//只剩下t==0的情况了，就是周围没有雷的位置
		for (int r = 1; r <= _MaxRanks; ++r) {
			for (int c = 1; c <= _MaxRanks; ++c) {

				if (getRowColAddr(r , c) == mb) {
					__Array * arr = __Array::create();
					arr->retain();
					NoAroundMine(r , c , arr);
					Ref *obj = NULL;
					CCARRAYDATA_FOREACH(arr->data , obj)
					{
						MineBlock * noMine = (MineBlock*)obj;
					//	if (noMine->getValue() == -2) {
					//		noMine->setTexture(CCTextureCache::getInstance()->getTextureForKey("0.png"));
					//	}
					//	else {
							sprintf(filename , "%d.png" , noMine->getValue());
							noMine->setTexture(TextureCache::getInstance()->getTextureForKey(filename));
					//	}

					}
					_count += arr->count();	//扫区计数加一
					arr->release();
					goto CONTINUEGAME;	//到此结束
				}//	end if...

			}
		}

	}	//end CCARRAYDATA_FOREACH...
	CONTINUEGAME:	//goto到此区判断游戏是否继续
	_isContinue=ContinueGame();
}

///////////////////////////////////////////////////////////////////
//	周边无雷块的连带显示
void GameScene::NoAroundMine(int row , int col , CCArray * arr)
{
	if (row == 0 || col == 0 || row == _MaxRanks + 1 || col == _MaxRanks + 1) {
		return;	//不需要设置的位置
	}
	MineBlock * mb = getRowColAddr(row , col);
	if (mb->getValue() == -1 || mb->getDig()) {
		return;	//是雷或者被挖开，直接返回退出
	}


	//CCLog("row = %d  col = %d " , row , col);
	//	此处有问题，为何不可以获取？
	//CCTexture2D * texture = CCTextureCache::sharedTextureCache()->textureForKey("0.png");

	//	设置当前的纹理
	//getRowColAddr(row , col)->setTexture(CCTextureCache::sharedTextureCache()->textureForKey("0.png"));

	arr->addObject(mb);
	mb->setDig(true);
	if (mb->getValue() != 0) {
		return;	//不是0，不再继续
	}
	//getRowColAddr(row , col)->setValue(-2);	//方便终止递归
	//设置四周纹理
	NoAroundMine(row + 1 , col + 1 , arr);
	NoAroundMine(row + 1 , col , arr);
	NoAroundMine(row + 1 , col - 1 , arr);
	NoAroundMine(row , col + 1 , arr);
	NoAroundMine(row , col - 1 , arr);
	NoAroundMine(row - 1 , col + 1 , arr);
	NoAroundMine(row - 1 , col , arr);
	NoAroundMine(row - 1 , col - 1 , arr);
}
