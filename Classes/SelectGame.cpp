#include "SelectGame.h"
#include "GameScene.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"

using namespace cocostudio::timeline;

bool SelectGame::init()
{
	if (!LayerColor::initWithColor(Color4B(64 , 76 , 128 , 255))) {
		return false;
	}
	
	//从csb文件中获取图层
	auto rootNode = CSLoader::createNode("select.csb");
	addChild(rootNode);
	//获取按钮，关联回调函数
	auto easyItem = static_cast<ui::Button*>(rootNode->getChildByName("Beasy"));
	easyItem->addTouchEventListener(CC_CALLBACK_1(SelectGame::MenuEasyCallBack , this));

	auto midItem = static_cast<ui::Button*>(rootNode->getChildByName("Bmid"));
	midItem->addTouchEventListener(CC_CALLBACK_1(SelectGame::MenuMidCallBack , this));

	auto hardItem = static_cast<ui::Button*>(rootNode->getChildByName("Bhard"));
	hardItem->addTouchEventListener(CC_CALLBACK_1(SelectGame::MenuHardCallBack , this));

	auto exitItem = static_cast<ui::Button*>(rootNode->getChildByName("Bexit"));
	exitItem->addTouchEventListener(CC_CALLBACK_1(SelectGame::MenuExitCallBack , this));
	return true;
}

void SelectGame::MenuEasyCallBack(Ref *)
{
	Scene * scene = GameScene::scene(1);
	Director::getInstance()->replaceScene(scene);

}

void SelectGame::MenuMidCallBack(Ref *)
{
	Scene * scene = GameScene::scene(2);
	Director::getInstance()->replaceScene(scene);
}

void SelectGame::MenuHardCallBack(Ref *)
{
	Scene * scene = GameScene::scene(3);
	Director::getInstance()->replaceScene(scene);
}

void SelectGame::MenuExitCallBack(Ref *)
{
	Director::getInstance()->end();
}

